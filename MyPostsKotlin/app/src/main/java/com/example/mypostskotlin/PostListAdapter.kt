package com.example.mypostskotlin

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class PostListAdapter(private val dataSet: List<PostModel>, val apiurl:String,
                      val userId: String?, var onItemClick: (post: PostModel) -> Unit,
                      var onDeleteClick: (post: PostModel) -> Unit) :
    RecyclerView.Adapter<PostListAdapter.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView
        val imageView: ImageView
        val deleteButton: Button

        init {
            // Define click listener for the ViewHolder's View.
            textView = view.findViewById(R.id.textViewPostTitle)
            imageView = view.findViewById(R.id.imageViewPost)
            deleteButton = view.findViewById(R.id.buttonDelete)

        }

    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.list_item, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.textView.text = dataSet[position].title
        var url = dataSet[position].imagePath
        //url = url.replace("http://localhost", R.string.apiUrl.toString())
        url = url.replace("http://localhost:3000", apiurl)
        url = url.replace("http://192.168.1.9", apiurl)
        LoadNetworkImage(viewHolder.imageView).execute(url)

        if(dataSet[position].creator != userId){

            viewHolder.deleteButton.setVisibility(View.INVISIBLE);

        }else{

            viewHolder.deleteButton.setOnClickListener{
                onDeleteClick(dataSet[position])
            }
        }

        viewHolder?.itemView?.setOnClickListener {

            onItemClick(dataSet[position])

        }


    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

}