package com.example.mypostskotlin

class PostModel {
    var _id: String =  ""
    var title: String =  ""
    var content: String =  ""
    var imagePath: String =  ""
    var creator: String =  ""
}