package com.example.mypostskotlin

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.util.Log
import android.widget.ImageView

class LoadNetworkImage(var imageView: ImageView) : AsyncTask<String, Void, Bitmap?>() {


    override fun doInBackground(vararg urls: String): Bitmap? {
        val imageURL = urls[0]
        var image: Bitmap? = null
        try {
            val `in` = java.net.URL(imageURL).openStream()
            image = BitmapFactory.decodeStream(`in`)
        }
        catch (e: Exception) {
            //Log.e("Error Message", e.message.toString())
           // e.printStackTrace()

            return null
        }

        return image
    }

    override fun onPostExecute(result: Bitmap?) {

        if(result!= null){
            imageView.setImageBitmap(result)
        }

    }

}