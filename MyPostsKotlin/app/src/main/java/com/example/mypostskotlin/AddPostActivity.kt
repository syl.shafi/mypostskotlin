package com.example.mypostskotlin

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.database.Cursor
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_add_post.*
import okhttp3.*
import java.io.File
import java.io.UnsupportedEncodingException
import java.net.UnknownHostException


class AddPostActivity : AppCompatActivity() {

    val REQUEST_IMAGE_CAPTURE = 0
    var imageBitmap: Bitmap? = null
    var imagePathName:String = ""
    var apiUrl: String = ""
    var editStat: Boolean = false
    private val sharedPrefFile = "kotlinsharedpreference"

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_post)

        val policy =
            StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        apiUrl = getString(R.string.apiUrl) + "/api/posts/"



        var _id = intent.extras?.getString("_id")

        if(_id != null){

            var title = intent.extras?.get("title").toString()
            var content = intent.extras?.get("content").toString()
            var imagePath = intent.extras?.get("imagePath").toString()


            editTextTitle.setText(title)
            editTextContent.setText(content)
            imagePath = imagePath.replace("http://localhost:3000", getString(R.string.apiUrl))
            imagePath = imagePath.replace("http://192.168.1.9", getString(R.string.apiUrl))

            LoadNetworkImage(imageViewPost).execute(imagePath)

            editStat = true

        }


        buttonImage.setOnClickListener{
            val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(takePicture, REQUEST_IMAGE_CAPTURE)
        }


        buttonAddPost.setOnClickListener{

            var title = editTextTitle.text.toString()
            var content = editTextContent.text.toString()

            if(title.isEmpty() || content.isEmpty()){
                Toast.makeText(this, "Add valid values in title and content.", Toast.LENGTH_SHORT).show()

            }
            else if(imageBitmap == null && !editStat){
                Toast.makeText(this, "Add Image.", Toast.LENGTH_SHORT).show()

            }
            else{

                if(!editStat){
                    uploadPost(title, content, imagePathName)
                }
                else {

                    editPost(_id, title, content, imagePathName)

               }




            }




        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            imageBitmap = data?.extras?.get("data") as Bitmap

            val Uri = getImageUri(applicationContext, imageBitmap)

            imagePathName = getRealPathFromURI(Uri)

            imageViewPost.setImageBitmap(imageBitmap)
        }
    }

    fun getImageUri(inContext: Context, inImage: Bitmap?): Uri? {
        val OutImage = Bitmap.createScaledBitmap(inImage!!, 1000, 1000, true)
        val path = MediaStore.Images.Media.insertImage(
            inContext.contentResolver,
            OutImage,
            "Title",
            null
        )
        return Uri.parse(path)
    }

    fun getRealPathFromURI(uri: Uri?): String {
        var path = ""
        if (contentResolver != null) {
            val cursor: Cursor? = uri?.let { contentResolver.query(it, null, null, null, null) }
            if (cursor != null) {
                cursor.moveToFirst()
                val idx: Int = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                path = cursor.getString(idx)
                cursor.close()
            }
        }
        return path
    }

    fun uploadPost(title: String?, content:String?, sourceImageFile: String) {
        try {

            val sourceFile = File(sourceImageFile)
            val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile,
                Context.MODE_PRIVATE)

            //Determining the media type
            val MEDIA_TYPE: MediaType? =
                if (sourceImageFile.endsWith("png")) MediaType.parse("image/png") else MediaType.parse(
                    "image/jpeg"
                )
            val requestBody: RequestBody = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(
                    "image",
                    sourceFile.name,
                    RequestBody.create(MEDIA_TYPE, sourceFile)
                )
                .addFormDataPart("title", title)
                .addFormDataPart("content", content)
                .build()

            val request: Request = Request.Builder()
                .header("Authorization", "bearer " +
                        sharedPreferences.getString("token", ""))
                .url(apiUrl)
                .post(requestBody)
                .build()
            val client = OkHttpClient()
            val response: Response = client.newCall(request).execute()

            runOnUiThread{
                var intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }


        } catch (e: UnknownHostException) {

            e.printStackTrace()

        } catch (e: UnsupportedEncodingException) {

            e.printStackTrace()

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }


    fun editPost(_id: String?, title: String?, content:String?, sourceImageFile: String) {

        try {

            val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile,
                Context.MODE_PRIVATE)

            var requestBody: RequestBody

            //println("hhhhhhhhhhhhhhhhhhhhhhhhhhhh")
            //println(sourceImageFile)
            //println("hhhhhhhhhhhhhhhhhhhhhhhhhhhh")

            if(!sourceImageFile.isEmpty()){

                val sourceFile = File(sourceImageFile)

                //Determining the media type
                val MEDIA_TYPE: MediaType? =
                    if (sourceImageFile.endsWith("png")) MediaType.parse("image/png") else MediaType.parse(
                        "image/jpeg"
                    )
                 requestBody = MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart(
                        "image",
                        sourceFile.name,
                        RequestBody.create(MEDIA_TYPE, sourceFile)
                    )
                    .addFormDataPart("id", _id)
                    .addFormDataPart("title", title)
                    .addFormDataPart("content", content)
                    .build()

            }else{
                requestBody = MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("id", _id)
                    .addFormDataPart("title", title)
                    .addFormDataPart("content", content)
                    .build()
            }

            var editUrl = apiUrl+ _id

            val request: Request = Request.Builder()
                .header("Authorization", "bearer " +
                        sharedPreferences.getString("token", ""))
                .url(editUrl)
                .put(requestBody)
                .build()
            val client = OkHttpClient()
            val response: Response = client.newCall(request).execute()

            runOnUiThread{
                var intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }


        } catch (e: UnknownHostException) {

            e.printStackTrace()

        } catch (e: UnsupportedEncodingException) {

            e.printStackTrace()

        } catch (e: Exception) {

            e.printStackTrace()

        }

    }





}
