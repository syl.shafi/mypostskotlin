package com.example.mypostskotlin

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.StrictMode
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.File
import java.io.IOException


class MainActivity : AppCompatActivity() {

    private lateinit var adapter: PostListAdapter
    private val sharedPrefFile = "kotlinsharedpreference"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        var apiUrl = getString(R.string.apiUrl) + "/api"
        val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile,Context.MODE_PRIVATE)

        val request = Request.Builder().url(apiUrl+"/posts").build()
        val client = OkHttpClient()


        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                TODO("Not yet implemented")
            }

            override fun onResponse(call: Call, response: Response) {
                    val body = response.body()?.string()
                    //println(body)

                    val gson = GsonBuilder().create()
                    var data = gson.fromJson(body, PostListModel::class.java)

                   //println(data.message)


                runOnUiThread {


                    adapter = PostListAdapter(data.posts, getString(R.string.apiUrl),
                        sharedPreferences.getString("userId", ""), ::onItemClick, ::onDeleteClick)
                    recyclerViewPost.adapter = adapter

                    var layoutManager = LinearLayoutManager(applicationContext,
                        RecyclerView.VERTICAL, false)

                    recyclerViewPost.layoutManager = layoutManager

                }


            }

        })

        if(sharedPreferences.getString("token", "") == ""){
            floatingActionButtonAddPost.hide()
        }


        floatingActionButtonAddPost.setOnClickListener{

            var intent = Intent(this, AddPostActivity::class.java)
            startActivity(intent)
        }


    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater

        val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile,Context.MODE_PRIVATE)

        if(sharedPreferences.getString("token", "") == ""){
            inflater.inflate(R.menu.main_menu, menu)
        }
        else{
            inflater.inflate(R.menu.partial_menu, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if(item.itemId == R.id.menu_refresh
            || item.itemId == R.id.menu_refresh_partial) {

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)

        }


        if(item.itemId == R.id.menu_login) {

            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)

        }

        if(item.itemId ==  R.id.menu_signup ){

            val intent = Intent(this, SignupActivity::class.java)
            startActivity(intent)

        }

        if(item.itemId ==  R.id.menu_logout ){

            val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile,Context.MODE_PRIVATE)

            val editor = sharedPreferences.edit()
            editor.clear()
            editor.apply()

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)

        }

        return true

    }

    fun onItemClick(post: PostModel){

        val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile,Context.MODE_PRIVATE)

        if(sharedPreferences.getString("userId", "") != post.creator){

            val builder = AlertDialog.Builder(this)
            //set title for alert dialog
            builder.setTitle(post.title)
            //set message for alert dialog
            builder.setMessage(post.content)

            //performing cancel action
            builder.setNeutralButton("Cancel"){dialogInterface , which ->
            }

            // Create the AlertDialog
            val alertDialog: AlertDialog = builder.create()
            // Set other dialog properties
            alertDialog.setCancelable(false)
            alertDialog.show()
        }
        else{

            var intent = Intent(this, AddPostActivity::class.java)

            intent.putExtra("_id", post._id)
            intent.putExtra("title", post.title)
            intent.putExtra("content", post.content)
            intent.putExtra("imagePath", post.imagePath)

            startActivity(intent)

        }


    }


    fun onDeleteClick(post: PostModel){

        val policy =
            StrictMode.ThreadPolicy.Builder().permitAll().build()

        StrictMode.setThreadPolicy(policy)


        val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile,Context.MODE_PRIVATE)

        var apiUrl = getString(R.string.apiUrl) + "/api/posts/"

        var deleteUrl = apiUrl+post._id

        val request = Request.Builder()
            .header("Authorization", "bearer " +
                    sharedPreferences.getString("token", ""))
            .url(deleteUrl)
            .delete()
            .build()

        val client = OkHttpClient()
        val response: Response = client.newCall(request).execute()

        var intent = Intent(this, MainActivity::class.java)

        startActivity(intent)

    }

}
