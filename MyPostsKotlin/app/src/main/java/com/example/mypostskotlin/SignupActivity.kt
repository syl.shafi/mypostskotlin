package com.example.mypostskotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_signup.*
import okhttp3.*
import java.io.IOException

class SignupActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)


        buttonSignup.setOnClickListener{

            var username = editTextSignupUserName.text.toString()
            var password = editTextSignupPassword.text.toString()

            if(username.isEmpty() || password.isEmpty()){
                textViewSignupError.text = "Enter valid credentials."
            }
            else if(!isEmailValid(username)){
                textViewSignupError.text = "Enter valid credentials."
            }

            else{

                var apiUrl = getString(R.string.apiUrl) + "/api/user/"

                var regUrl = apiUrl+"signup"

                val formBody = FormBody.Builder()
                    .add("email", username)
                    .add("password", password)
                    .build()

                val request = Request.Builder()
                    .url(regUrl)
                    .post(formBody)
                    .build()

                val client = OkHttpClient()

                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        TODO("Not yet implemented")
                    }

                    override fun onResponse(call: Call, response: Response) {

                        val body = response.body()?.string()
                        println(body)

                        val gson = GsonBuilder().create()
                        var data = gson.fromJson(body, AuthModel::class.java)

                        if(data.result == null){
                            runOnUiThread {
                                textViewSignupError.text = "Invalid Username."
                            }
                        }
                        else{

                            runOnUiThread{
                                var intent = Intent(applicationContext, LoginActivity::class.java)
                                startActivity(intent)
                            }

                        }

                    }
                })


            }

        }

    }

    fun isEmailValid(email: CharSequence?): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }


}
