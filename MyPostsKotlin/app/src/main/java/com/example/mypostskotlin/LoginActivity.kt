package com.example.mypostskotlin

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Patterns
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.*
import java.io.IOException


class LoginActivity : AppCompatActivity() {

    private val sharedPrefFile = "kotlinsharedpreference"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile,Context.MODE_PRIVATE)

        val timer: CountDownTimer =
            object : CountDownTimer(3590 * 1000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    //Some code
                }

                override fun onFinish() {
                    val editor = sharedPreferences.edit()
                    editor.clear()
                    editor.apply()

                    runOnUiThread{
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        startActivity(intent)
                    }
                }
            }

        buttonLogin.setOnClickListener{

            var username = editTextUserName.text.toString()
            var password = editTextPassword.text.toString()

            if(username.isEmpty() || password.isEmpty()){
                textViewLoginError.text = "Enter valid credentials."
            }
            else if(!isEmailValid(username)){
                textViewLoginError.text = "Enter valid credentials."
            }
            else{

                var apiUrl = getString(R.string.apiUrl) + "/api/user/"

                var loginUrl = apiUrl+"login"

                val formBody = FormBody.Builder()
                    .add("email", username)
                    .add("password", password)
                    .build()

                val request = Request.Builder()
                    .url(loginUrl)
                    .post(formBody)
                    .build()

                val client = OkHttpClient()

                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        TODO("Not yet implemented")
                    }

                    override fun onResponse(call: Call, response: Response) {

                        val body = response.body()?.string()
                        //println(body)

                        val gson = GsonBuilder().create()
                        var data = gson.fromJson(body, AuthModel::class.java)

                        if(data.token.isEmpty()){
                            runOnUiThread {
                                textViewLoginError.text = "Invalid Username or Password."
                            }
                        }
                        else{

                            val editor:SharedPreferences.Editor =  sharedPreferences.edit()

                            editor.putString("userId", data.userId)
                            editor.putString("token", data.token)
                            editor.putString("expiresIn", data.expiresIn)
                            editor.apply()
                            editor.commit()

                            timer.start()

                            runOnUiThread{
                               var intent = Intent(applicationContext, MainActivity::class.java)
                               startActivity(intent)
                           }

                        }

                    }
                })


            }

        }

    }

    fun isEmailValid(email: CharSequence?): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }


}
