package com.example.mypostskotlin

import java.util.*

class AuthModel {

    var token:String = ""
    var message:String = ""
    var userId:String = ""
    var expiresIn:String = ""
    var result:Object? = null
}